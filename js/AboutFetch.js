/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
function getAuthors(authorLabel) {
    var authorString = ""
    readFile("qrc:/credits/authorsFile", function(jsonTXT) {
        var jsonAuthors = JSON.parse(jsonTXT).authors
        for (var i = 0; i < jsonAuthors.length; i++) {
            authorString = authorString + jsonAuthors[i].name + " (" + jsonAuthors[i].nick + ")" + "<br><font color=\"#B5B5B5\">URL: " + jsonAuthors[i].url + "</font>"
            if (i !== jsonAuthors.length - 1) {
                authorString = authorString + "<br>"
            }
        }
        authorLabel.text = authorString
    })
}
function getCopyrightAuthors(authorLabel, years) {
    var authorString = "Copyright © " + years + " "
    readFile("qrc:/credits/authorsFile", function(jsonTXT) {
        var jsonAuthors = JSON.parse(jsonTXT).authors
        for (var i = 0; i < jsonAuthors.length; i++) {
            authorString = authorString + jsonAuthors[i].name
            if (i >= jsonAuthors.length - 2) {
                if (i === jsonAuthors.length - 2) {
                    authorString = authorString + " & "
                }
                else {
                    authorString = authorString + "."
                }
            }
            else {
                authorString = authorString + ", "
            }
        }
        authorLabel.text = authorString
    })
}

function getTranslators(translatorsLabel, localeHelper) {
    var translatorString = ""
    readFile("qrc:/credits/translatorsFile", function(jsonTXT) {
        var jsonTranslations = JSON.parse(jsonTXT).translations
        for (var i = 0; i < jsonTranslations.length; i++) {
            translatorString = translatorString + localeHelper.getLanguageName(jsonTranslations[i].language) +" " + " (" + Qt.locale(jsonTranslations[i].language).nativeLanguageName + "):<br>"
            for (var j = 0; j < jsonTranslations[i].translators.length; j++) {
                translatorString = translatorString + "<font color=\"#B5B5B5\">" + jsonTranslations[i].translators[j] + "</font>"
                if (j !== jsonTranslations[i].translators.length - 1) {
                    translatorString = translatorString + "<br>"
                }
            }
            if (i !== jsonTranslations.length - 1) {
                translatorString = translatorString + "<br>"
            }
        }
        translatorsLabel.text = translatorString
    })
}

function readFile(url, callback){
    var xhr = new XMLHttpRequest;
    xhr.open("GET", url); // set Method and File
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE){ // if request_status == DONE
            var response = xhr.responseText;
            if (typeof callback === 'function') {
                callback(response)
            }
        }
    }
    xhr.send(); // begin the request
}
