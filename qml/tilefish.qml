/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow
{
    id: appWindow
    // expose appWindow as 'app' to other qml files
    property ApplicationWindow app: appWindow

    //: App name
    //% "Tilefish"
    property string appName: qsTrId("tilefish-coverlabel")
    // executable name
    property string execName: "tilefish"

    //: Shown on Cover when no pages exists
    //% "No Pages."
    property string noPagesCoverText: qsTrId("tilefish-cover-no-pages")
    //: prefix for Pages. eq. "Page" 1 / *
    //% "Page"
    property string pageNameText: qsTrId("tilefish-page-title-text")

    //: Panes as in "Panes": 4
    //% "Panes"
    property string panesText: qsTrId("tilefish-panes")

    //: prefix for images. eq. "Images" : 4
    //% "Images"
    property string imagesText: qsTrId("tilefish-images")

    property string currentPage: noPagesCoverText
    property string currentPagePanes: ""
    property string currentPageImages: ""

    initialPage: Component { MainPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.Portrait
    function coverNext() {
        console.log("adsd")
    }
}
