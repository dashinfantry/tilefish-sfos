/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover
    Column {
        anchors.centerIn: parent
        width: parent.width
        height: parent.height - (parent.height / 8)
        Label {
            id: appName
            anchors.horizontalCenter: parent.horizontalCenter
            text: app.appName
        }
        Image {
            anchors.margins: appName.height
            width: parent.width / 2
            height: parent.width / 2
            anchors.horizontalCenter: parent.horizontalCenter
            source: "/usr/share/icons/hicolor/172x172/apps/tilefish.png"
        }
        Label {
            id: pageCount
            anchors.horizontalCenter: parent.horizontalCenter
            text: app.currentPage
        }
        Label {
            id: pagePaneCount
            anchors.horizontalCenter: parent.horizontalCenter
            text: app.currentPagePanes
        }
        Label {
            id: pageImageCount
            anchors.horizontalCenter: parent.horizontalCenter
            text: app.currentPageImages
        }

    }/*
    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-next"
            onTriggered: {
                coverNext()
                appWindow.activate()
            }
        }
    }*/
}
