/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import Sailfish.Silica 1.0

import "../../js/addPageSliderEqualizer.js" as SliderEqualizer

Dialog {
    id: page

    property string name: "Page Name"
    property int columns: 1
    property int rows: 1
    property int items: 1

    Column {
        width: parent.width

        DialogHeader {
            //: Add Page -text.
            //% "Add Page..."
            title: qsTrId("tilefish-add-page")
        }
        Slider {
            //: Columns -slider label
            //% "Columns"
            label: qsTrId("tilefish-columns")
            id: columnSlider
            width: parent.width
            maximumValue: 4
            minimumValue: 1
            value: 1
            valueText: value
            stepSize: 1
            onValueChanged: SliderEqualizer.syncSliders(columnSlider, rowSlider, panesLabel)
        }
        Slider {
            //: Rows -slider label
            //% "Rows"
            label: qsTrId("tilefish-rows")
            id: rowSlider
            width: parent.width
            maximumValue: 4
            minimumValue: 1
            value: 1
            valueText: value
            stepSize: 1
            onValueChanged: SliderEqualizer.syncSliders(columnSlider, rowSlider, panesLabel)
        }
        Label {
            leftPadding: Theme.horizontalPageMargin
            id: panesText
            text: app.panesText + ": "
            font.pixelSize: Theme.fontSizeExtraLarge
            color: Theme.highlightColor
            Label {
                anchors.left: panesText.right
                anchors.top: panesText.top
                id: panesLabel
                text: "1"
                font.pixelSize: Theme.fontSizeExtraLarge
            }
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            columns = columnSlider.value
            rows = rowSlider.value
            items = parseInt(panesLabel.text, 10)
        }
    }
}
